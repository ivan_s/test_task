start:
1) clone project
2) virtualenv -p python3.6 .
3) source bin/activate
4) pip install -r requirements.txt
5) python run.py