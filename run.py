
import os

from create_archive import create_zip_archives
from process_files import process_files_and_create_csv

ROOT_DIR = ''   # Можно изменить на /tmp
ZIPFILE_DIR = 'zip_files'

ZIPFILE_COUNT = 50
XMLFILE_COUNT = 100


if __name__ == '__main__':
    print('Создание {zipfile_count} zip-архивов, в каждом {xmlfile_count} xml'
          ' файлов со случайными данными'.format(zipfile_count=ZIPFILE_COUNT,
                                                 xmlfile_count=XMLFILE_COUNT))
    create_zip_archives(path_to_zip_dir=os.path.join(ROOT_DIR, ZIPFILE_DIR),
                        zipfile_count=ZIPFILE_COUNT,
                        xmlfile_count=XMLFILE_COUNT)

    print('Обработка директории с полученными zip архивами, '
          'разбивка вложенных xml файлов и формирование 2 csv файлов')
    process_files_and_create_csv(root_dir=ROOT_DIR, zipfile_dir=ZIPFILE_DIR)