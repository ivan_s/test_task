
import os
import csv
from zipfile import ZipFile
from multiprocessing import Pool, Process, cpu_count

from lxml import etree

from utils import timer


@timer
def process_archive_dir(path_to_dir):
    """
        Обработка директории с полученными zip архивами(
            используя мультипроцессинг)
    """
    paths_to_archive = [os.path.join(path_to_dir, archive_name)
                        for archive_name in os.listdir(path_to_dir)]

    try:
        proc_num = cpu_count()
    except NotImplementedError:
        proc_num = 2
    pool = Pool(processes=proc_num)
    results = pool.map(process_archive, paths_to_archive)
    pool.close()
    pool.join()
    return results


def process_archive(path_to_archive):
    """
        Обработка данных zip архива
    """
    results = []
    with ZipFile(path_to_archive) as zipfile:
        for filename in zipfile.namelist():
            with zipfile.open(filename) as xmlfile:
                xml_data = xmlfile.read()
                tree = etree.XML(xml_data)
                results.append({
                    'id': tree.xpath('/root/var[@name="id"]')[0].get('value'),
                    'level': tree.xpath('/root/var[@name="level"]')[0].get(
                        'value'),
                    'object_names': [obj.get('name') for obj in tree.xpath(
                        '/root/objects/object')]
                })
    return results


@timer
def create_csv_file(data, root_dir):
    """
        Создание двух csv файлов в параллельных процессах
    """
    def create_id_level_file(data, root_dir):
        csv_filename = os.path.join(root_dir, 'id_level_file.csv')
        with open(csv_filename, 'w') as id_level_file:
            id_level_writer = csv.DictWriter(id_level_file,
                                             fieldnames=('id', 'level'))
            id_level_writer.writeheader()
            for arhive_result in data:
                for item in arhive_result:
                    id_level_writer.writerow({
                        'id': item['id'], 'level': item['level']
                    })

    def create_id_object_name_file(data, root_dir):
        csv_filename = os.path.join(root_dir, 'id_object_name_file.csv')
        with open(csv_filename, 'w') as id_object_name_file:
            id_object_name_writter = csv.DictWriter(
                id_object_name_file, fieldnames=('id',  'object_name'))
            id_object_name_writter.writeheader()
            for arhive_result in data:
                for item in arhive_result:
                    for object_name in item['object_names']:
                        id_object_name_writter.writerow({
                            'id': item['id'],
                            'object_name': object_name
                        })

    p1 = Process(target=create_id_level_file,
                 kwargs={'data': data, 'root_dir': root_dir})
    p2 = Process(target=create_id_object_name_file,
                 kwargs={'data': data, 'root_dir': root_dir})
    p1.start()
    p2.start()

    p1.join()
    p2.join()


# @timer
# def create_csv_file_alternative(data):
#     """
#         Этот вариант дольше выполняется (0.22 секунды против 0.18),
#         чем параллельное создание двух csv файлов,
#         но возможно лучше подходит для дальнейших модификаций
#     """
#     id_level_file = open('id_level_file.csv', 'w')
#     id_level_writer = csv.DictWriter(id_level_file, fieldnames=('id', 'level'))
#     id_level_writer.writeheader()
#
#     id_object_name_file = open('id_object_name_file.csv', 'w')
#     id_object_name_writter = csv.DictWriter(id_object_name_file,
#                                             fieldnames=('id',  'object_name'))
#     id_object_name_writter.writeheader()
#     for archive_result in data:
#         for item in archive_result:
#             id_level_writer.writerow({'id': item['id'], 'level': item['level']})
#             for object_name in item['object_names']:
#                 id_object_name_writter.writerow({
#                     'id': item['id'],
#                     'object_name': object_name
#                 })
#     id_level_file.close()
#     id_object_name_file.close()


@timer
def process_files_and_create_csv(root_dir, zipfile_dir):
    """
        Обработка директории с полученными zip архивами и создание csv файлов
    """
    process_result = process_archive_dir(
        path_to_dir=os.path.join(root_dir, zipfile_dir))
    create_csv_file(data=process_result, root_dir=root_dir)
