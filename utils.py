import time
import string
from random import sample


def timer(f):
    """
        Декоратор для измерения времени работы функции
    """
    def tmp(*args, **kwargs):
        start_time = time.time()
        res = f(*args, **kwargs)
        end_time = time.time()
        print("Время выполнения функции %s: %f секунд" % (f.__name__,
                                                          end_time-start_time))
        return res
    return tmp


def generate_random_string(string_len):
    """
        Генерация случайной строки из букв англ. алфавита и цифр
    """
    random_string = sample(string.ascii_letters + string.digits, string_len)
    return str(''.join(random_string))
