
import os
import zipfile
from uuid import uuid4
from random import randint

from lxml import etree
from lxml.builder import E

from utils import generate_random_string

ZIP_COMPRESSION_METHOD = zipfile.ZIP_DEFLATED

ZIPFILE_NAME_PREFIX = 'archive_'

RANDOM_STRING_LEN = 10
RANDOM_OBJECTS_TAGS = 10
RANDOM_INT_RANGE = (0, 100)
RANDOM_OBJECTS_TAGS_RANGE = (1, 10)


def generate_xml_file_data():
    """
        Генерация данных для xml файла
    """
    id_tag = E.var({'name': 'id', 'value': str(uuid4())})
    level_tag = E.var({
        'name': 'level',
        'value': str(randint(*RANDOM_INT_RANGE))
    })
    objects_tags = []
    for i in range(randint(*RANDOM_OBJECTS_TAGS_RANGE)):
        objects_tags.append(E.object({
            'name': generate_random_string(RANDOM_STRING_LEN)
        }))
    xml_file = E.root(id_tag, level_tag, E.objects(*objects_tags))
    return etree.tostring(xml_file, encoding='utf-8')


def create_zip_archive(path_to_archive, xmlfile_count):
    """
        Создание zip архива с xml файлами
    """
    with zipfile.ZipFile(path_to_archive, 'w',
                         ZIP_COMPRESSION_METHOD) as zip_archive:
        for i in range(xmlfile_count):
            xml_data = generate_xml_file_data()
            zip_archive.writestr(str(i) + '.xml', xml_data)


def create_zip_archives(path_to_zip_dir, zipfile_count, xmlfile_count):
    if not os.path.exists(path_to_zip_dir):
        os.makedirs(path_to_zip_dir)
    for i in range(zipfile_count):
        path_to_archive = os.path.join(
            path_to_zip_dir, ''.join([ZIPFILE_NAME_PREFIX, str(i), '.zip']))
        create_zip_archive(path_to_archive=path_to_archive,
                           xmlfile_count=xmlfile_count)
